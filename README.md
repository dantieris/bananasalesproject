# README #

This README intends to show an overview of our product, pinpointing some features of it.

### What is this repository for? ###

* This repository is meant to be made to store the artifacts and some images which represent the initial idea and the solutions provided by the team.

* Version 1.0

### How do I get set up? ###

* In order to use the files of this project you need to install the following programs:
        -GitBash
        -Astah Professional
        -Eclipse
        -Microsoft Word and Excel

### Who do I talk to? ###

* In case you need to talk to some of the administrators to discuss the nature of this project, please contact:

https://bitbucket.org/masmangan<br/>

https://bitbucket.org/tarcisiomonteiro<br/>

https://bitbucket.org/dantieris<br/>

https://bitbucket.org/flipd58<br/>

