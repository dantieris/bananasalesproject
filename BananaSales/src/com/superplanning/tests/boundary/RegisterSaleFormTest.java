package com.superplanning.tests.boundary;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.superplanning.boundary.RegisterSaleForm;

public class RegisterSaleFormTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testSartSaleEnableInsertProduct() {
		RegisterSaleForm form = new RegisterSaleForm();
		
		form.start();
		
		assertTrue(form.bt_insertProduct.isEnabled());
	}
	
	@Test
	public void testStartSaleEnableFinish() {
		RegisterSaleForm form = new RegisterSaleForm();
		
		form.start();
		
		System.out.println(form.bt_finish.isEnabled());
		
		assertTrue(form.bt_finish.isEnabled());
	}
	
	@Test
	public void testStartSaleDisanableStart() {
		RegisterSaleForm form = new RegisterSaleForm();
		
		form.start();
		
		assertTrue(!form.bt_start.isEnabled());
	}
	
	@Test
	public void testStartSaleEnableRegisterSale() {
		RegisterSaleForm form = new RegisterSaleForm();
		
		form.start();
		
		assertTrue(form.registerSale != null);
	}

	@Test
	public void testInserProductIdHamburgerAloneAndCleanTextField() {
		RegisterSaleForm form = new RegisterSaleForm();
		
		form.insertProductId(Integer.parseInt(form.tf_insertProduct.getText()));
		
		assertEquals(form.registerSale.getLastProduct().getId() == 2603509 &&
					form.tf_insertProduct.getText() == "");
	}
	
	@Test
	public void testInserProductIdSetLastProductLabelProductName() {
		RegisterSaleForm form = new RegisterSaleForm();
		
		form.insertProductId(Integer.parseInt(form.tf_insertProduct.getText()));
		
		assertEquals(form.lb_lastProductId.getText() == form.registerSale.getLastProduct().getName());
	}
	
	@Test
	public void testInserProductIdValueLabelHasProductValue() {
		RegisterSaleForm form = new RegisterSaleForm();
		
		form.insertProductId(Integer.parseInt(form.tf_insertProduct.getText()));
		
		assertEquals(form.lb_value.getText() == String.valueOf(form.registerSale.getLastProduct().getValue()));
	}
	
	@Test
	public void testInserProductIdSubtotalLabelHasProductValue() {
		RegisterSaleForm form = new RegisterSaleForm();
		
		form.insertProductId(Integer.parseInt(form.tf_insertProduct.getText()));
		
		assertEquals(form.lb_subtotal.getText() == String.valueOf(form.registerSale.getLastProduct().getValue()));
	}
}
