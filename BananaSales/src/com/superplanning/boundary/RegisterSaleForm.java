package com.superplanning.boundary;

 
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import com.superplanning.controller.RegisterSale;


public class RegisterSaleForm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 179615214886964968L;
	private JPanel contentPane;
	private JTextField tf_productId;
	private JTextField tf_subtotal;
	private JTextField tf_total;
	private JTextField tf_payment;
	private JTextField tf_change;
	public RegisterSale registerSale;
	public JButton bt_start, bt_finish, bt_confirm, bt_emmitTicket, bt_insertProduct;
	private JLabel lb_productId, lb_subtotal, lb_total, lb_payment, lb_change, lb_value, lb_lastProduct;
	private JTextArea ta_ticket;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterSaleForm frame = new RegisterSaleForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegisterSaleForm() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 437, 481);
		
		this.setResizable(false);
		
		initLabels();
		initButtons();
		initTextFiels();
		initProductList();
		initComponents();		
	}
	
	private void initComponents() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Add ticket
		contentPane.add(ta_ticket);
		
		// Add text fields
		contentPane.add(tf_total);
		contentPane.add(tf_subtotal);
		contentPane.add(tf_change);
		contentPane.add(tf_payment);
		contentPane.add(tf_productId);
		
		// Add labels
		contentPane.add(lb_productId);
		contentPane.add(lb_change);
		contentPane.add(lb_payment);
		contentPane.add(lb_total);
		contentPane.add(lb_value);
		contentPane.add(lb_subtotal);
		contentPane.add(lb_lastProduct);
		
		// Add buttons
		
		contentPane.add(bt_insertProduct);
		contentPane.add(bt_start);
		contentPane.add(bt_finish);
		contentPane.add(bt_emmitTicket);
		contentPane.add(bt_confirm);
	}

	private void initProductList() {
		ta_ticket = new JTextArea("Name		Value\n");
		ta_ticket.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		ta_ticket.setBounds(25, 92, 220, 270);
		ta_ticket.setEditable(false);
//		ta_ticket.setEnabled(false);
		
		hamburgerTest();
	}
	
	private void hamburgerTest() {
		String ticket = ta_ticket.getText();
		
		String hamburger = "Hamburger		37,90\n";
		
		ta_ticket.setText(ticket+hamburger);
		
		tf_productId.setText("2603509");
		tf_subtotal.setText("37,90");
		tf_total.setText("37,90");
		tf_payment.setText("50,00");
		tf_change.setText("10,10");
		lb_value.setText("37,90 R$");
		lb_lastProduct.setText("Hamburger");
		
	}
	
	private void breadTest() {
		String ticket = ta_ticket.getText();
		
		String bread = "P�o Preto		4,99";
		
		ta_ticket.setText(ticket+bread);
		
		tf_productId.setText("2603526"); // Product ID
		tf_subtotal.setText("4,99"); // Subtotal value
		tf_total.setText("4,99"); // Total value
		tf_payment.setText("10,00"); // Payment value
		tf_change.setText("5,01"); // Change value
		lb_value.setText("4,99 R$"); // Las
		lb_lastProduct.setText("P�o Preto");
	}
	
	private void bisnaguinhaTest() {
		String ticket = ta_ticket.getText();
		
		String bisnaguinha = "Bisnaguinha		3,39";
		
		ta_ticket.setText(ticket+bisnaguinha);
		
		tf_productId.setText("2603452");
		tf_subtotal.setText("37,90");
		tf_total.setText("37,90");
		tf_payment.setText("50,00");
		tf_change.setText("10,10");
		lb_value.setText("37,90 R$");
		lb_lastProduct.setText("Hamburger");
	}

	private void initTextFiels() {
		Font font = new Font("Tahoma", Font.BOLD, 16);
		
		tf_subtotal = new JTextField("0,00");
		tf_subtotal.setFont(font);
		tf_subtotal.setBounds(272, 117, 86, 20);
		tf_subtotal.setColumns(10);
		tf_subtotal.setEditable(false);
//		tf_subtotal.setEnabled(false);

		tf_total = new JTextField("0,00");
		tf_total.setFont(font);
		tf_total.setBounds(272, 173, 86, 20);
		tf_total.setColumns(10);
		tf_total.setEditable(false);
//		tf_total.setEnabled(false);
		
		tf_payment = new JTextField("0,00");
		tf_payment.setFont(font);
		tf_payment.setBounds(272, 229, 86, 20);
		tf_payment.setColumns(10);
		tf_payment.setEditable(false);
//		tf_payment.setEnabled(false);

		tf_change = new JTextField("0,00");
		tf_change.setFont(font);
		tf_change.setBounds(272, 296, 86, 20);
		tf_change.setColumns(10);
		tf_change.setEditable(false);
//		tf_change.setEnabled(false);
		
		tf_productId = new JTextField("");
		tf_productId.setFont(font);
		tf_productId.setBounds(29, 45, 100, 20);
		tf_productId.setColumns(10);
		tf_productId.setEditable(false);
//		tf_productId.setEnabled(false);
	}

	public void initButtons() {
		bt_start = new JButton("Start");
		bt_start.setBounds(45, 376, 70, 23);
		bt_start.addActionListener(null);
		bt_start.setEnabled(true);
		
		bt_finish = new JButton("Finish");
		bt_finish.setBounds(45, 410, 70, 23);
		bt_finish.addActionListener(null);
		bt_finish.setEnabled(false);
		
		bt_confirm = new JButton("Confirm");
		bt_confirm.setBounds(121, 410, 81, 23);
		bt_confirm.addActionListener(null);
		bt_confirm.setEnabled(false);
		
		bt_emmitTicket = new JButton("Ticket");
		bt_emmitTicket.setBounds(121, 376, 81, 23);
		bt_emmitTicket.addActionListener(null);
		bt_emmitTicket.setEnabled(false);
		
		bt_insertProduct = new JButton("+");
		bt_insertProduct.setBounds(258, 47, 114, 23);
		bt_insertProduct.addActionListener(null);
		bt_insertProduct.setEnabled(false);
	}
	
	public void initLabels() {
		Font font = new Font("Tahoma", Font.BOLD, 16);
		
		lb_productId = new JLabel("Product ID");
		lb_productId.setFont(font);
		lb_productId.setBounds(30, 20, 100, 14);
		lb_productId.setLabelFor(tf_productId);
		
		lb_change = new JLabel("Change");
		lb_change.setFont(font);
		lb_change.setBounds(272, 265, 86, 20);
		
		lb_total = new JLabel("Total");
		lb_total.setFont(font);
		lb_total.setBounds(272, 148, 46, 14);
		
		lb_payment = new JLabel("Payment");
		lb_payment.setFont(font);
		lb_payment.setBounds(272, 204, 86, 20);
		
		lb_subtotal = new JLabel("Sub-Total");
		lb_subtotal.setFont(font);
		lb_subtotal.setBounds(272, 92, 100, 14);
		
		lb_value = new JLabel(" R$");
		lb_value.setBackground(Color.WHITE);
		lb_value.setFont(font);
		lb_value.setBounds(132, 47, 81, 14);
		
		lb_lastProduct = new JLabel("");
		lb_lastProduct.setBackground(Color.WHITE);
		lb_lastProduct.setFont(font);
		lb_lastProduct.setBounds(30, 70, 100, 20);
	}

	public void start() {
		bt_insertProduct.setEnabled(true);
		bt_start.setEnabled(false);
		bt_finish.setEnabled(true);
		
		registerSale = new RegisterSale();
	}
	
	
 }