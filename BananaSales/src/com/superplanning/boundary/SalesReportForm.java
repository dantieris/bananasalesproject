package com.superplanning.boundary;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.BorderLayout;
import javax.swing.JInternalFrame;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JList;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.JTree;
import javax.swing.DropMode;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

public class SalesReportForm {

	private JFrame frame;
	private JTextField txtReportConcerningNovember;
	private JTable table;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SalesReportForm window = new SalesReportForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SalesReportForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 563, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblHotDog = new JLabel("Hot Dog");
		lblHotDog.setBounds(159, 96, 57, 20);
		frame.getContentPane().add(lblHotDog);
		
		JLabel lblShampoo = new JLabel("Shampoo");
		lblShampoo.setBounds(159, 69, 57, 20);
		frame.getContentPane().add(lblShampoo);
		
		JLabel lblMayonese = new JLabel("Ketchup");
		lblMayonese.setBounds(159, 124, 57, 20);
		frame.getContentPane().add(lblMayonese);
		
		JLabel lblPizza = new JLabel("Pizza");
		lblPizza.setBounds(159, 43, 36, 20);
		frame.getContentPane().add(lblPizza);
		
		JLabel lblSoap = new JLabel("Soap");
		lblSoap.setBounds(159, 148, 36, 20);
		frame.getContentPane().add(lblSoap);
		
		JLabel lblR_8 = new JLabel("R$12,00");
		lblR_8.setBounds(489, 122, 48, 20);
		frame.getContentPane().add(lblR_8);
		
		JLabel lblR_9 = new JLabel("R$15,00");
		lblR_9.setBounds(489, 148, 48, 20);
		frame.getContentPane().add(lblR_9);
		
		JLabel lblR_6 = new JLabel("R$40,00");
		lblR_6.setBounds(488, 69, 48, 20);
		frame.getContentPane().add(lblR_6);
		
		JLabel lblR_7 = new JLabel("R$4,00");
		lblR_7.setBounds(489, 96, 48, 20);
		frame.getContentPane().add(lblR_7);
		
		JLabel lblR_5 = new JLabel("R$40,00");
		lblR_5.setBounds(488, 43, 48, 20);
		frame.getContentPane().add(lblR_5);
		
		JLabel lblR_2 = new JLabel("R$3,00");
		lblR_2.setBounds(326, 148, 48, 20);
		frame.getContentPane().add(lblR_2);
		
		JLabel lblR_4 = new JLabel("R$2,00");
		lblR_4.setBounds(326, 96, 48, 20);
		frame.getContentPane().add(lblR_4);
		
		JLabel lblR_3 = new JLabel("R$4,00");
		lblR_3.setBounds(326, 122, 48, 20);
		frame.getContentPane().add(lblR_3);
		
		JLabel lblR_1 = new JLabel("R$10,00");
		lblR_1.setBounds(325, 69, 48, 20);
		frame.getContentPane().add(lblR_1);
		
		JLabel lblR = new JLabel("R$20,00");
		lblR.setBounds(325, 43, 48, 20);
		frame.getContentPane().add(lblR);
		
		JLabel label_20 = new JLabel("3");
		label_20.setBounds(410, 124, 17, 20);
		frame.getContentPane().add(label_20);
		
		JLabel label_21 = new JLabel("4");
		label_21.setBounds(410, 69, 17, 20);
		frame.getContentPane().add(label_21);
		
		JLabel label_19 = new JLabel("5");
		label_19.setBounds(410, 148, 17, 20);
		frame.getContentPane().add(label_19);
		
		JLabel label_22 = new JLabel("2");
		label_22.setBounds(410, 96, 17, 20);
		frame.getContentPane().add(label_22);
		
		JLabel label_23 = new JLabel("2");
		label_23.setBounds(410, 43, 17, 20);
		frame.getContentPane().add(label_23);
		
		JLabel label_9 = new JLabel("68799");
		label_9.setBounds(247, 148, 36, 20);
		frame.getContentPane().add(label_9);
		
		JLabel label_11 = new JLabel("520933");
		label_11.setBounds(247, 69, 57, 20);
		frame.getContentPane().add(label_11);
		
		JLabel label_10 = new JLabel("4567");
		label_10.setBounds(247, 124, 36, 20);
		frame.getContentPane().add(label_10);
		
		JLabel label_13 = new JLabel("2345");
		label_13.setBounds(247, 43, 36, 20);
		frame.getContentPane().add(label_13);
		
		JLabel label_12 = new JLabel("14567");
		label_12.setBounds(247, 96, 36, 20);
		frame.getContentPane().add(label_12);
		
		JLabel label_8 = new JLabel("1");
		label_8.setBounds(99, 148, 17, 20);
		frame.getContentPane().add(label_8);
		
		JLabel label_7 = new JLabel("1");
		label_7.setBounds(99, 124, 17, 20);
		frame.getContentPane().add(label_7);
		
		JLabel label_6 = new JLabel("1");
		label_6.setBounds(99, 69, 17, 20);
		frame.getContentPane().add(label_6);
		
		JLabel label_5 = new JLabel("1");
		label_5.setBounds(99, 96, 17, 20);
		frame.getContentPane().add(label_5);
		
		JLabel label_4 = new JLabel("1");
		label_4.setBounds(99, 43, 17, 20);
		frame.getContentPane().add(label_4);
		
		JLabel label_3 = new JLabel("13/11/2014");
		label_3.setBounds(4, 148, 58, 30);
		frame.getContentPane().add(label_3);
		
		JLabel lblNewLabel = new JLabel("12/11/2014");
		lblNewLabel.setBounds(3, 38, 58, 30);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel label_2 = new JLabel("13/11/2014");
		label_2.setBounds(4, 119, 58, 30);
		frame.getContentPane().add(label_2);
		
		JLabel label = new JLabel("12/11/2014");
		label.setBounds(4, 64, 58, 30);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("12/11/2014");
		label_1.setBounds(4, 91, 58, 30);
		frame.getContentPane().add(label_1);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setForeground(Color.BLACK);
		separator_3.setBackground(Color.BLACK);
		separator_3.setBounds(314, 0, 1, 196);
		frame.getContentPane().add(separator_3);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setOrientation(SwingConstants.VERTICAL);
		separator_5.setForeground(Color.BLACK);
		separator_5.setBackground(Color.BLACK);
		separator_5.setBounds(456, 0, 1, 196);
		frame.getContentPane().add(separator_5);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setOrientation(SwingConstants.VERTICAL);
		separator_4.setForeground(Color.BLACK);
		separator_4.setBackground(Color.BLACK);
		separator_4.setBounds(383, 0, 1, 196);
		frame.getContentPane().add(separator_4);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setForeground(Color.BLACK);
		separator_2.setBackground(Color.BLACK);
		separator_2.setBounds(226, 0, 1, 196);
		frame.getContentPane().add(separator_2);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setForeground(Color.BLACK);
		separator_1.setBackground(Color.BLACK);
		separator_1.setBounds(149, 0, 1, 196);
		frame.getContentPane().add(separator_1);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setForeground(Color.BLACK);
		separator.setBackground(Color.BLACK);
		separator.setBounds(61, 0, 1, 196);
		frame.getContentPane().add(separator);
		
		JTextPane TextArea = new JTextPane();
		TextArea.setBounds(0, 241, 545, 20);
		TextArea.setText("SisVendas                                     ---------------------------------------------------------                                 28/112014");
		frame.getContentPane().add(TextArea);
		
		JButton btnNewButton = new JButton("New Period");
		btnNewButton.setBackground(Color.CYAN);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(456, 210, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnBack = new JButton("Back");
		btnBack.setBackground(Color.CYAN);
		btnBack.setBounds(4, 210, 89, 23);
		frame.getContentPane().add(btnBack);
		
		JTextArea txtrDate = new JTextArea();
		txtrDate.setText("DATE");
		txtrDate.setBounds(0, 0, 63, 38);
		frame.getContentPane().add(txtrDate);
		
		JTextArea txtrCashierid = new JTextArea();
		txtrCashierid.setText("CASHIER_ID");
		txtrCashierid.setBounds(61, 0, 89, 38);
		frame.getContentPane().add(txtrCashierid);
		
		JTextArea txtrProduct = new JTextArea();
		txtrProduct.setText(" PRODUCT\r\n");
		txtrProduct.setBounds(149, 0, 80, 38);
		frame.getContentPane().add(txtrProduct);
		
		JTextArea txtrProductid = new JTextArea();
		txtrProductid.setText("PRODUCT_ID");
		txtrProductid.setBounds(226, 0, 102, 38);
		frame.getContentPane().add(txtrProductid);
		
		JTextArea txtrValue = new JTextArea();
		txtrValue.setText("VALUE");
		txtrValue.setBounds(326, 0, 58, 38);
		frame.getContentPane().add(txtrValue);
		
		JTextArea txtrQuantity = new JTextArea();
		txtrQuantity.setText("QUANTITY");
		txtrQuantity.setBounds(383, 0, 80, 38);
		frame.getContentPane().add(txtrQuantity);
		
		JTextArea txtrTo = new JTextArea();
		txtrTo.setText("TOTAL_SALE");
		txtrTo.setBounds(463, 0, 82, 38);
		frame.getContentPane().add(txtrTo);
		
		table = new JTable();
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		table.setForeground(Color.LIGHT_GRAY);
		table.setBackground(Color.LIGHT_GRAY);
		table.setBounds(0, 38, 545, 158);
		frame.getContentPane().add(table);
		
	}
}
